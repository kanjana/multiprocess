#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
int arr[10] = {2,4,1,6,5,3,7,9,8,0};
int max;
int avr;
void DoWorkInChild(){
	int x;
	int i;
	printf("Child process is working\n");
	max = arr[0];
	for(x=0;x<0xFFFFFFF;x++){ //The biggest int value
		for(i=1;i<10;i++){
			if(max < arr[i]){
				max = arr[i];		
			}
		}
	}
      printf("Child Data:  max= %d \n",max);
}
void DoWorkInParent(){
	int x;
	int i;
	int sum;
	printf("Parent process is working\n");
	for(x=0;x<0xFFFFFFF;x++){
		sum=0;
		for(i=0;i<10;i++){
			sum=sum+arr[i];
		}
		avr=sum/10;
	}
}
void main (){
	pid_t pids[10];
	int i;
	int n = 1;

	/* Start children. */
	for (i = 0; i < n; ++i) {
		pids[i] = fork();
	/*
	 * Return value from fork system call
	 * Negative value: Creation of child process was unsuccessful.
	 * Zero: Returned to the newly created child process.
	 * Positive value: Returned to parent. The value contains child process id.
	 */
		printf("Pid = %d , i = %d\n",pids[i],i);	
  		if (pids[i] < 0) {
    			perror("fork");
    			abort();
		//Creation of child process was unsuccessful
  		} else {
			if (pids[i] == 0) {
    				DoWorkInChild();
   			 	exit(0);
  			}
		}
	}
     DoWorkInParent();

	/* Wait for children to exit. */
	int status;
	pid_t pid;
	while (n > 0) {
  		pid = wait(&status);
		//Parent wait until child process done
  		printf("Child with PID %ld exited with status 0x%x.\n", (long)pid, status);
  		--n;  // 
	}
    printf("Parrent Data:  max= %d  avr = %d\n",max,avr);
}
