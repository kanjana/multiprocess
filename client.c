#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
int main()
{
    const char *name ="OS";
    int shm_fd;
    void *ptr;
    shm_fd=shm_open(name,O_RDONLY,0666);
    ptr=mmap(0,10,PROT_READ,MAP_SHARED,shm_fd,0);
    printf("%s",(char *)ptr);
    return 0;
}


