#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>

///////////////gcc -o m mutliprocess_1.c -lrt

int arr[10] = {2,4,1,6,5,3,7,9,8,0};
int max=0;
int avr=0;
int shm_fd;
int *ptr1=&max;
int *ptr2=&avr;
const char *name1 ="MAX";
const char *name2 ="AVR";
void *ptr;
void DoWorkInChild(){
	int x;
	int i;
	for(x=0;x<0xFFFFFFF;x++){
		for(i=1;i<10;i++){
			if(max < arr[i]){
				max = arr[i];		
			}
		}
	}

	shm_fd=shm_open(name1,O_CREAT |O_RDWR,0666);
      ftruncate(shm_fd,4);
      ptr=mmap(0,3,PROT_WRITE,MAP_SHARED,shm_fd,0);
      sprintf(ptr,"%d",max);
      printf("Child Data:  max= %d  avr = %d\n",max,avr);
}
void DoWorkInParent(){
	int x;
	int i;
	int sum;
	for(x=0;x<0xFFFFFFF;x++){
		sum=0;
		for(i=0;i<10;i++){
			sum=sum+arr[i];
		}
		avr=sum/10;
	}
}
void main (){
	pid_t pids[10];
	int i;
	int n = 1;

	/* Start children. */
	for (i = 0; i < n; ++i) {
		pids[i] = fork();
		printf("pid = %d , i = %d\n",pids[i],i);	
  		if (pids[i] < 0) {
    			perror("fork");
    			abort();
  		} else {
			if (pids[i] == 0) {
    				DoWorkInChild();
   			 	exit(0);
  			}
		}
	}
     DoWorkInParent();

	/* Wait for children to exit. */
	int status;
	pid_t pid;
	while (n > 0) {
  		pid = wait(&status);
  		--n;
	}
    shm_fd=shm_open(name1,O_RDONLY,0666);
    ptr=mmap(0,3,PROT_READ,MAP_SHARED,shm_fd,0);
    printf("Shared Data = %s\n",(char *)ptr);
   char *tmpMAX=(char*)ptr;
    printf("Parrent Data:  max= %d  avr = %d   max for child =%s\n",max,avr,tmpMAX);
}
