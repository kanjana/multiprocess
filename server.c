#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
int main()
{
    const char *name ="OS";
    int shm_fd;
    void *ptr;
    shm_fd=shm_open(name,O_CREAT |O_RDWR,0666);
    //Create shared memory object named OS
    ftruncate(shm_fd,40);
    ptr=mmap(0,10,PROT_WRITE,MAP_SHARED,shm_fd,0);
    sprintf(ptr,"Hello!! This is calling from server\n");
    while(1); //forever loop
    return 0;
}
//*gcc -o server server.c -lrt *//

